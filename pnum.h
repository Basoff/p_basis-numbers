#ifndef _P_NUM
#define _P_NUM

#include <cmath>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using namespace std;

class PnumException {};

class P_num{
	int basis;
	vector <int> value;
	string letters [6] = {"A", "B", "C", "D", "E", "F"};
	bool isCorrect();
public:
	~P_num();
	int decimal() const;
	P_num(int num, int bas);
	P_num(const P_num &num);
	P_num(const P_num &num, int bas);
	P_num operator + (const P_num &n) const;
	P_num& operator += (const P_num &n);
	P_num operator - () const;
	P_num operator - (const P_num &n) const;
	P_num& operator -= (const P_num &n);
	P_num operator * (const P_num &n) const;
	P_num& operator *= (const P_num &n);
	P_num operator * (const int n) const;
	P_num& operator *= (const int n);
	P_num operator / (const P_num &n) const;
	P_num& operator /= (const P_num &n);
	
	friend istream& operator >> (istream& in, P_num &n);
	friend ostream& operator << (ostream& out, const P_num &r);
	
	//additional task
	P_num square ();
	P_num sqr ();
};

#endif