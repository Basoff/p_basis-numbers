#include <iostream>
#include "pnum.h"
/* run this program using the console pauser or add your own getch, system("pause") or input loop */
using namespace std;

int main(int argc, char** argv) {
	
	//cout and constructor checkup
	cout<<"|cout and constructor checkup|\n";
	P_num var1 (0x64, 5);
	cout << "var1: " << var1 << "=" << var1.decimal()<<endl<<endl;
	
	//copy constructor checkup
	cout<<"|copy constructor checkup|\n";
	P_num var2 (var1);
	cout << "var2: " << var2 << "=" << var2.decimal()<<endl<<endl;
	
	//plus checkup
	cout<<"|plus checkup|\n";
	P_num var3 (05, 8);
	cout << "var3: " << var3 << "=" << var3.decimal()<<endl;
	cout << "var3 + var1: " << var3+var1 << "=" <<(var3+var1).decimal()<<endl;
	cout << "var1 + var3: " << var1+var3 << "=" <<(var1+var3).decimal()<<endl<<endl;
	
	//plus+equals checkup
	cout<<"|plus+equals checkup|\n";
	P_num var4 (0, 3);
	cout << "var4: " << var4 << "=" << var4.decimal()<<endl;
	var4 += var2;
	cout << "var4 += var2: var4=" << var4 << "=" << var4.decimal()<<endl<<endl;
	
	//minus checkup
	cout<<"|minus checkup|\n";
	P_num var5 (87, 2);
	cout << "var5: " << var5 << "=" << var5.decimal()<<endl;
	cout << "var5 - var3: " << var5-var3 << "=" <<(var5-var3).decimal()<<endl;
	cout << "var1 - var5: " << var1-var5 << "=" <<(var1-var5).decimal()<<endl<<endl;
	
	//minus+equals checkup
	cout<<"|minus+equals checkup|\n";
	var4 -= var5;
	cout << "var4-=var5: var4=" << var4 << "=" << var4.decimal()<<endl<<endl;
	
	//multiplying checkup
	cout<<"|multiplying checkup|\n";
	P_num var6 (10, 5);
	cout << "var6: " << var6 << "=" << var6.decimal()<<endl;
	cout << "var6 * var3: " << var6*var3 << "=" <<(var6*var3).decimal()<<endl;
	cout << "var3 * var6: " << var3*var6 << "=" <<(var3*var6).decimal()<<endl<<endl;
	
	//multiplying+equals checkup
	cout<<"|multiplying+equals checkup|\n";
	var6 *= var3;
	cout << "var6*=var3: var6=" << var6 << "=" << var6.decimal()<<endl<<endl;
	
	//deviding test
	cout<<"|dividing checkup|\n";
	cout <<"var6/var3 = "<<var6/var3 <<"=" <<(var6/var3).decimal()<<endl<<endl;
	var6 /= var3;
	cout <<"var6/=var3 = " << var6 << "=" << var6.decimal()<<endl<<endl;
	
	//letter test
	cout<<"|letter view checkup|\n";
	P_num var7 (0x8e, 16);
	cout <<var7<<endl<<endl;
	
	//square functions test
	cout<<"|Sqr and sqrt checkup|\n";
	P_num var_square (var6.square());
	cout<<var_square<<"="<<(var_square).decimal()<<endl;
	cout<<var_square.sqr()<<"="<<(var_square.sqr()).decimal()<<endl<<endl;
	
	//hex input chekup
	cout<<"|hex&oct input checkup|\n";
	P_num var_hex (0xabc, 4);
	cout<<"Hex input type: "<<var_hex<<"	"<<var_hex.decimal()<<endl;
	cout<<-var_hex<<"	"<<-var_hex.decimal()<<endl<<endl;
	P_num var_oct (0777, 7);
	cout<<"Oct input type: "<<var_oct<<"	"<<var_oct.decimal()<<endl;
	cout<<-var_oct<<"	"<<-var_oct.decimal()<<endl<<endl;
		
	//square equasion test
	cout<<"|square equasion checkup|\n";
	int number, basis, option;
	
	cout << "Choose number input format for number 1:\n 1)DEC\n 2)HEX\n 3)OCT\n :"; cin >> option;
	while (option < 1 || option > 3){
		cout << "Choose one:\n 1)DEC\n 2)HEX\n 3)OCT\n :"; cin >> option;
	}
	cout<<"Enter the first number:";
	switch (option) {
		case 1:
			cin >> number >> basis;
			break;
		case 2:
			cin >> hex >> number >> dec >> basis;
			break;
		case 3:
			cin >> oct >> number >> dec >> basis;
			break;
	};
	P_num var_a (number, basis);
	
	cout << "Choose number input format for number 2:\n 1)DEC\n 2)HEX\n 3)OCT\n :"; cin >> option;
	while (option < 1 || option > 3){
		cout << "Choose one:\n 1)DEC\n 2)HEX\n 3)OCT\n :"; cin >> option;
	}
	cout<<"Enter the second number:";
	switch (option) {
		case 1:
			cin >> number >> basis;
			break;
		case 2:
			cin >> hex >> number >> dec >> basis;
			break;
		case 3:
			cin >> oct >> number >> dec >> basis;
			break;
	};
	P_num var_b (number, basis);
	
	cout << "Choose number input format for number 3:\n 1)DEC\n 2)HEX\n 3)OCT\n :"; cin >> option;
	while (option < 1 || option > 3){
		cout << "Choose one:\n 1)DEC\n 2)HEX\n 3)OCT\n :"; cin >> option;
	}
	cout<<"Enter the third number:";
	switch (option) {
		case 1:
			cin >> number >> basis;
			break;
		case 2:
			cin >> hex >> number >> dec >> basis;
			break;
		case 3:
			cin >> oct >> number >> dec >> basis;
			break;
	};
	P_num var_c (number, basis);
	
	cout<<"Enter the basis of the system you'd like the answer to be:";
	cin >> basis;
	
	P_num D (var_b.square() - (var_a*var_c*4));
	cout<<"D = "<<D<<"	"<<D.decimal()<<endl;
	if (D.decimal() >= 0){
		if (D.decimal() > 0){
			P_num d (D.sqr());
			cout<<"Vd = "<<d<<"	"<<d.decimal()<<endl;
			P_num x1 ((-var_b + d)/(var_a*2), basis);
			cout<<"x1 = "<<x1<<endl;
			P_num x2 (-((var_b + d)/(var_a*2)), basis);
			cout<<"x2 = "<<x2<<endl;
		}
		else{
			P_num x ((-var_b)/(var_a*2), basis);
			cout<<"x = "<<x<<"	"<<x.decimal()<<endl;
		}
	}
	else cout<<"Discriminant is below zero. No natural roots.\n";
	
	
	
	return 0;
}