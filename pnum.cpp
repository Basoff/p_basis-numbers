#include "pnum.h"

bool P_num::isCorrect(){
	for (int i = 0; i < this->value.size(); i++){
		if (this->value[i] >= this->basis) return false;
	}
	return true;
}

P_num::~P_num(){
	value.clear();
}

int P_num::decimal()const{
	int result = 0;
//	cout<<"=="<<*this<<"==\n\n";
	for (int i = value.size(); i > 0; i --){
		if (value[i-1] >= 0)
			result += value[i-1] * pow(basis, value.size()-i);
		else{
			result -= value[i-1] * pow(basis, value.size()-i);
			result *= -1;
//		cout << result << "	" << value[i-1] <<"	" << basis <<"	"<< value.size()-i<<endl;	
		}
	}

	return result;
}

P_num::P_num(int num, int bas){
	//cout<<"'"<<num<<"'\n\n\n";
	bool isNegative = false;
	if (bas <=16 && bas >= 2)
		basis = bas;
	else throw PnumException();
	if (num < 0){
		isNegative = true;
		num *= -1;
	}
	
	while (num/bas > 0){
//cout<<"num="<<num<<"	bas="<<bas<<"	num%bas="<<num%bas<<endl;
		value.insert(value.begin(), num%bas);
		num/=bas;
	}
	if (isNegative) num *= -1;
	this->value.insert(this->value.begin(), num);
}

P_num::P_num(const P_num &num){
	basis = num.basis;
	value = num.value;
}

P_num::P_num(const P_num &num, int bas){
	if (bas >= 2 && bas <= 16) basis = bas;
	else throw PnumException();
	
	int numb = num.decimal();
	P_num add (numb, bas);
	value = add.value;
}

P_num P_num::operator + (const P_num &n) const{
	int num = decimal()+ n.decimal();
	return P_num(num, basis);
}

P_num& P_num::operator += (const P_num &n){
	int num = decimal()+ n.decimal();
	P_num a (num, this->basis);
	value = a.value;
	return *this;
} 

P_num P_num::operator - () const{
	int num = decimal() * -1;
	P_num res (num, basis);
	return res;
} 

P_num P_num::operator - (const P_num &n) const{
	int num = this->decimal() - n.decimal();
	return P_num(num, this->basis);
}

P_num& P_num::operator -= (const P_num &n){
	int num = decimal()- n.decimal();
	P_num a (num, basis);
	value = a.value;
	return *this;
} 

P_num P_num::operator * (const P_num &n) const{
	int num = this->decimal() * n.decimal();
	return P_num(num, this->basis);
}

P_num& P_num::operator *= (const P_num &n){
	int num = decimal()* n.decimal();
	P_num a (num, basis);
	value = a.value;
	return *this;
}

P_num P_num::operator * (const int n) const{
	int num = decimal() * n;
	return P_num(num, this->basis);
}

P_num& P_num::operator *= (const int n){
	int num = decimal()* n;
	P_num a (num, basis);
	value = a.value;
	return *this;
}

P_num P_num::operator / (const P_num &n) const{
	int num = decimal() / n.decimal();
	return P_num(num, basis);
}

P_num& P_num::operator /= (const P_num &n){
	int num = decimal()/ n.decimal();
	P_num a (num, basis);
	value = a.value;
	return *this;
}

//output
ostream& operator << (ostream& out, const P_num &n)
{
	for (int i = 0; i < n.value.size(); i++){
		if (n.value[i] >= 10){
			out << n.letters[n.value[i]-10];
		} 
		else out << n.value[i];
	}
	out <<"("<<n.basis<<")";
	return out;
}

//additional task
P_num P_num::square(){
	int num = decimal() * decimal();
	return P_num (num, basis);
}

P_num P_num::sqr(){
	int num = sqrt(decimal());
	return P_num (num, basis);
}